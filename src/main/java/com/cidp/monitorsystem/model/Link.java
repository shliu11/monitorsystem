package com.cidp.monitorsystem.model;

import java.util.Date;
import java.util.Objects;

/**
 * @date 2021/4/4 -- 16:40
 **/
public class Link {
    private String sifindex;
    private String difindex;
    private String sip;
    private String dip;
    private String sif;
    private String dif;
    private String sflow;
    private String dflow;
    private String sfRate;
    private String dfRate;
    private String date;


    public String getSifindex() {
        return sifindex;
    }

    public void setSifindex(String sifindex) {
        this.sifindex = sifindex;
    }

    public String getDifindex() {
        return difindex;
    }

    public void setDifindex(String difindex) {
        this.difindex = difindex;
    }

    public String getSip() {
        return sip;
    }

    public void setSip(String sip) {
        this.sip = sip;
    }

    public String getDip() {
        return dip;
    }

    public void setDip(String dip) {
        this.dip = dip;
    }

    public String getSif() {
        return sif;
    }

    public void setSif(String sif) {
        this.sif = sif;
    }

    public String getDif() {
        return dif;
    }

    public void setDif(String dif) {
        this.dif = dif;
    }

    public String getSflow() {
        return sflow;
    }

    public void setSflow(String sflow) {
        this.sflow = sflow;
    }

    public String getDflow() {
        return dflow;
    }

    public void setDflow(String dflow) {
        this.dflow = dflow;
    }

    public String getSfRate() {
        return sfRate;
    }

    public void setSfRate(String sfRate) {
        this.sfRate = sfRate;
    }

    public String getDfRate() {
        return dfRate;
    }

    public void setDfRate(String dfRate) {
        this.dfRate = dfRate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return (Objects.equals(sip, link.sip) &&
                Objects.equals(dip, link.dip))||
                (Objects.equals(sip, link.dip) &&
                        Objects.equals(dip, link.sip));
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public String toString() {
        return "Link{" +
                "sifindex='" + sifindex + '\'' +
                ", difindex='" + difindex + '\'' +
                ", sip='" + sip + '\'' +
                ", dip='" + dip + '\'' +
                ", sif='" + sif + '\'' +
                ", dif='" + dif + '\'' +
                ", sflow='" + sflow + '\'' +
                ", dflow='" + dflow + '\'' +
                ", sfRate='" + sfRate + '\'' +
                ", dfRate='" + dfRate + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
