package com.cidp.monitorsystem.model;

import java.util.List;

/**
 * @date 2021/4/4 -- 16:46
 **/
public class NodeData {
    private List<SystemInfo> systemInfo;
    private List<Cpu> cpu;
    private List<Memory> memory;


    public List<SystemInfo> getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(List<SystemInfo> systemInfo) {
        this.systemInfo = systemInfo;
    }

    public List<Cpu> getCpu() {
        return cpu;
    }

    public void setCpu(List<Cpu> cpu) {
        this.cpu = cpu;
    }

    public List<Memory> getMemory() {
        return memory;
    }

    public void setMemory(List<Memory> memory) {
        this.memory = memory;
    }

    @Override
    public String toString() {
        return "NodeData{" +
                "systemInfo=" + systemInfo +
                ", cpu=" + cpu +
                ", memory=" + memory +
                '}';
    }
}
